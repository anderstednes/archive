archive.org backup
==================

Two scripts to backup websites to [Internet Archive](https://archive.org/)
based on their RSS feed.

Installation:

```
$ sudo apt-get install tidy
$ pip3 install -r requirements.txt
```

Usage:

```
$ ./code/extract.py https://example.com/feed/ | xargs ./code/archive.py
```
